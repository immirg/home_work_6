import java.util.*;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> myList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int number = (int) (Math.random() * 21 - 10);
            myList.add(number);
        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number from -10 to 10 inclusive: ");
        int inputNumber = scanner.nextInt();
        System.out.println(myList + " Created collection using Random");
        System.out.println(overwrite(myList) + " New collection by overwriting");
        System.out.println(uniqueValues(myList) + " Create a collection with unique values");
        System.out.println(sortCollection(myList) + " Sorting the collection");
        System.out.println(findMinMax(myList) + " Finding the minimum and maximum");
        System.out.println(findSum(myList) + " Sum of the elements of a collection");
        System.out.println(positiveNumbers(myList) + " A collection containing only positive numbers");
        System.out.println(hasValue(myList, inputNumber) + " - Checking for the presence of a given number in a collection");
        System.out.println(countSpecificNumber(myList, inputNumber) + " Count the number of occurrences of a specific number");
        System.out.println(mirror(myList) + " Expanded collection in reverse order");
        System.out.println(swapElements(myList) + " The first and last element are swapped");
        System.out.println(secondLargestNumber(myList) + " Second largest number in the collection");
        System.out.println(checkPalindrome(myList) + " Checking if a list is palindromic");
        System.out.println("Spent " + countTime() + " seconds creating ArrayList and LinkedList");
    }
    public static ArrayList<Integer> overwrite(ArrayList<Integer> list){
        ArrayList <Integer> overWriteArrayList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++){
            overWriteArrayList.add(list.get(i));
        }
        return overWriteArrayList;
    }
    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list){
        ArrayList<Integer> removeDuplicates = new ArrayList<>();
        for (int a = 0; a < list.size(); a++){
            boolean duplicates = false;
            for (int b = 0; b < removeDuplicates.size(); b++){
                if (list.get(a) == removeDuplicates.get(b)){
                    duplicates = true;
                    break;
                }
            }
            if (!duplicates) removeDuplicates.add(list.get(a));
        }
        return removeDuplicates;
    }
    public static ArrayList<Integer> sortCollection(ArrayList<Integer> list){
        ArrayList<Integer> sort = new ArrayList<>();
        sort.addAll(list);
        int number;
        for (int a = 0; a < sort.size()-1; a++){
            for (int b = a+1; b < sort.size(); b++){
                if (sort.get(a) > sort.get(b)){
                    number = sort.get(a);
                    sort.set(a, sort.get(b));
                    sort.set(b, number);
                }
            }
        }
        return sort;
    }
    public static ArrayList<Integer> findMinMax(ArrayList<Integer> list){
        ArrayList<Integer> minMax = new ArrayList<>();
        minMax.add(0, 0);
        minMax.add(1, 0);
        for (int i = 0; i < list.size(); i++){
            if (minMax.get(0) > list.get(i)) minMax.set(0, list.get(i));
            if (minMax.get(1) < list.get(i)) minMax.set(1, list.get(i));
        }
        return minMax;
    }
    public static ArrayList<Integer> findSum(ArrayList<Integer> list){
        ArrayList<Integer> sumCollection = new ArrayList<>();
        int sum = 0;
        for (int i = 0; i < list.size(); i++){
            sum += list.get(i);
        }
        sumCollection.add(sum);
        return sumCollection;
    }
    public static ArrayList<Integer> positiveNumbers(ArrayList<Integer> list){
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < list.size(); i++){
            if (list.get(i) > 0){
                numbers.add(list.get(i));
            }
        }
        return numbers;
    }
    public static boolean hasValue(ArrayList<Integer> list, int value){
        for (int i = 0; i < list.size(); i++){
            if (value == list.get(i)) return true;
        }
        return false;
    }
    public static ArrayList<Integer> countSpecificNumber(ArrayList<Integer> list, int value){
        ArrayList<Integer> result = new ArrayList<>();
        int count = 0;
        for (int i = 0; i < list.size(); i++){
            if (list.get(i) == value) count += 1;
        }
        result.add(count);
        return result;
    }
    public static ArrayList<Integer> mirror(ArrayList<Integer> list){
        ArrayList<Integer> mirrorCollection = new ArrayList<>();
        for (int i = list.size()-1; i >= 0; i--){
            mirrorCollection.add(list.get(i));
        }
        return mirrorCollection;
    }
    public static ArrayList<Integer> swapElements(ArrayList<Integer> list){
        int number = list.get(0);
        list.set(0, list.get(list.size()-1));
        list.set(list.size()-1, number);
        return list;
    }
    public static ArrayList<Integer> secondLargestNumber(ArrayList<Integer> list) {
        // если в коллекции все чила одинаковые то secondMax будет равен -11, я не знаю как это можно исправить
        ArrayList<Integer> secondNum = new ArrayList<>();
        int firstMax = -11;
        int secondMax = -11;
        for (int num : list) {
            if (num > firstMax) {
                secondMax = firstMax;
                firstMax = num;
            }
            else if (num > secondMax && num != firstMax) {
                secondMax = num;
            }
        }
        secondNum.add(secondMax);
        return secondNum;
    }
    public static ArrayList<Boolean> checkPalindrome(ArrayList<Integer> list){
        ArrayList<Boolean> palindromeResults = new ArrayList<>();
        int len = list.size()-1;
        boolean palindrome = true;
        for (int a = 0, b = len; a <= len/2 && b >= len/2; a++, b--){
            if (list.get(a) != list.get(b)) palindrome = false;
        }
        palindromeResults.add(palindrome);
        return palindromeResults;
    }
    public static long countTime(){
        Date date1 = new Date();
        long beforeStart = date1.getTime()/1000;
        ArrayList<Integer> numArrayList = addElementInArrayList();
        ArrayList<Integer> randomSelectedLinkedList = randomSelectsArrayList(numArrayList);
        LinkedList<Integer> numLinkedList = addElementsInLinkedList();
        LinkedList<Integer> randomSelectedArrayList = randomSelectsLinkedList(numLinkedList);
        Date date2 = new Date();
        long afterFinish = date2.getTime()/1000;
        return (afterFinish - beforeStart);
    }
    public static ArrayList<Integer> addElementInArrayList(){
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++){
            list.add(i);
        }
        return list;
    }
    public static LinkedList<Integer> addElementsInLinkedList(){
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 0; i < 1000000; i++){
            list.add(i);
        }
        return list;
    }
    public static ArrayList<Integer> randomSelectsArrayList(ArrayList<Integer> list){
        ArrayList<Integer> random = new ArrayList<>();
        for (int i = 0; i < 100000; i++){
            int num = (int) (Math.random() * (100000)) + 1;
            random.add(list.get(num));
        }
        return random;
    }
    public static LinkedList<Integer> randomSelectsLinkedList(LinkedList<Integer> list){
        LinkedList<Integer> random = new LinkedList<>();
        for (int i = 0; i < 100000; i++){
            int num = (int) (Math.random() * (100000)) + 1;
            random.add(list.get(num));
        }
        return random;
    }
}
